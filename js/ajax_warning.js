// Drupal alerts "An HTTP error 0 occurred" during normal site operation,
// confusing site visitors/editors.
// @see http://drupal.org/node/1232416#comment-6165578
Drupal.behaviors.ajax_warning = function(context) {
  "use strict";
  $.ajaxSetup({
    beforeSend: function(jqXHR, settings) {
      settings.error = function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status || Drupal.settings.ajax_warning.enable_warnings) {
          // An AJAX HTTP error occurred.
          // Show error Message.
          alert(Drupal.ajaxError(jqXHR, settings.url));
        }
        // do nothing if AJAX HTTP request terminated abnormally.
      };
    }
  });
};
